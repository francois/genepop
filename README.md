# Genepop reference page

`Genepop` ([Rousset, 2008](http://dx.doi.org/10.1111/j.1471-8286.2007.01931.x)) is distributed both as an R package on [CRAN](https://cran.r-project.org/web/packages/genepop), and as a stand-alone executable on this page. Both are based on the (fonctionally) latest version of the `Genepop` C++ sources, currently version 4.8.2 or above (January-February 2023).

## The following files are all found in the `distribution` subdirectory of this repository:

Full version 4.8.3 distribution: download `GenepopV4.tar.gz` or `GenepopV4.zip`.

Documentation: download `Genepop4.8.pdf`

Version 4.8.3 Windows Executables (64-bit version): download `exe.zip` (recompiled on 2023/10/25, as problems have been reported with the previous compilation). Also included is a recompiled version of the `linkdos` program (Garnier-Géré and Dillmann 1992), which was distributed with previous versions of `Genepop`. `linkdos` is not part of `Genepop` itself.


Example files: download `examples.zip`.

Version 4.8.3 sources (Windows, Linux, Macintosh; also linkdos source): download `sources.tar.gz` or `sources.zip`

In case of problem with files obtained from this page, please report to me (not to the Genepop on the Web maintainer!); and read the documentation first.

Genepop is freeware (i.e. you don't need to pay). It is free software covered by the CeCILL licence (GPL compatible), i.e. it can be used, copied, studied, modified and redistributed in other free software also covered by a GPL-compatible licence (in particular, with freely available source code, even if commercial software), and provided the Genepop source is acknowledged (of course... well, that does not seem obvious to everyone. See the GPL!).

See the documentation for all further information.

© François Rousset 2007-now
