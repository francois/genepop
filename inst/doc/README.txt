*ARGH* The default when using devtools to perform the checks is that inst/doc is erased [so any change there is lost when checks are performed before a commit]
To avoid that one needs version >= 1.4.0 of pkgbuild (perhaps also a recent version of rcmdbuild) and the line
Config/build/clean-inst-doc: false
in DESCRIPTION

This is Roxygened so don't modify the Rd files...
The GenepopS.pdf must be recompacted (see below) after being regerated by knit... easily forgotten when bothered by NOTEs about urls... 

Make sure that the latest version of the R package (with the latest version of the C++ executable, for its version number), is installed
Workflow is
knit Genepop.Rmd to gitbook and to pdf_book (and check the version number in the title)

run tools::compactPDF() on GenepopS.pdf

before building archive
before running makeDistrib.ps1...


No GenepopS.tex should be used.

The source for the doc is inst/doc/GenepopS.Rmd
One may need to install bookdown.
*The html files are generated by compiling as gitbook, not html.*
*to generate the pdf doc, 
   * do not run latex through the 'compile PDF' button of Rstudio (this fails cryptically) 
   * run pdf_book through the knitR menu. Check that the section numbers and index are in the output. (note that knitR hides all temp files from the inst/doc/ directory... nice). (note that this re-creates a GenepopS.tex !?)

The .Rinstignore has doc/GenepopS[.]pdf
The inst/doc/.gitignore has GenepopS.pdf (yes, different syntax)

In the built source package tar.gz, the inst/doc package then has the html files from the /doc directory, the Rmd and md files... 

...and the inst/extdoc had files from the /extdoc directory, now removed from the sources:
It seems that the /extdoc directory is not currently (v1.1.7) useful and is removed.
The /doc appears appropriate ["Only help files, NEWS, DESCRIPTION and files under doc/and demo/in a package can be viewed" -- I have tried /demo in the past but then "Subdirectory 'demo' contains invalid file names: 'Genepop.Rmd'..."]

The version of this file in the Genepop_jlopez/ copy of genepop might contain more info about the logic of the package structure

problem with qpdf? Need to install the exe (rather than the R package 'qpdf') and see https://stackoverflow.com/questions/11738844/qpdf-exe-for-compactpdf for path issues.

